# CSS overrides

CSS overrides enable you to adjust the appearance of a popular electronic research administration application.

# Deploy

To deploy these overrides, create a CSS override. Copy some or all of the CSS rules here to your CSS override. Make sure you mark your CSS override active.

# Test

CSS overrides will appear only on certain, enabled pages, such as the version 15 new home page. Once your rule is created and active, view the home page. You should see your CSS styles embedded in the head section of the HTML document.

Ex. 

<pre>
&lt;head&gt;
...
&lt;style type="text/css"&gt;.home .k-grid-pager .k-icon {
    top: initial;
}&lt;/style&gt;
&lt;/head&gt;
&lt;body&gt;
...
</pre>

